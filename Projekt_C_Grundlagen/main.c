#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

// Definieren der Gobalen Variabeln
int temp;
int zeit;
char zeitTemp[100];
char tempTemp[100];
char firstLine[100];
char druck[1024];
char semi[] = ";";
char fileName[30];

// Initialisieren Funktionen
int files_suchen();
void fileEinlesen(char name[30]);
int endungpruefen(char name[30]);
void zeitTempLesen (char name[100]);

void printWartungsMenu();
int printMenu();
void printpreHeatMenu();
void printPrintMenu();
void readFile();

int main() {
        int option = 0;
        while(option!=4){
            option = printMenu();
                switch (option) {
                    case 1:
                        printPrintMenu();
                        break;
                    case 2:
                        printpreHeatMenu();
                        break;
                    case 3:
                        printWartungsMenu();
                        break;
                    case 4:
                        printf("Drucker wird ausgeschalten\n");
                }
        }


}

int printMenu() {
    printf("Funktionsmenu\n");
    printf("==============\n");
    printf(" 1 - Drucken\n");
    printf(" 2 - Vorheizen\n");
    printf(" 3 - Wartung\n");
    printf(" 4 - Ausschalten\n");
    int option;
    scanf("%d", &option);
    return option;
}

void printWartungsMenu() {

    int sollTemperaturOben = 240;
    int sollTemperaturUnten = 25;
    int istTemperatur = 5;
    printf("Wartungsmenu\n");
    printf("==============\n\n");
    printf("Oeffne Deckel...\n");
    printf("Bestaetigen mit beliebiger Taste...\n\n");
    char ch;
    scanf("%c", &ch);
    while (getchar() != '\n');
    while (istTemperatur <= sollTemperaturOben) {
        printf("\n Temperatur: %d", istTemperatur);
        istTemperatur += 5;
    }
    printf("\n Spule dreht links 3-5s");
    printf("\n Spulenwechsel");
    printf("\n Bestaetigen mit beliebiger Taste...\n\n");
    scanf("%c", &ch);
    while (getchar() != '\n');
    printf("\n Spule dreht rechts");
    while (istTemperatur >= sollTemperaturUnten) {
        printf("\n Temperatur: %d", istTemperatur);
        istTemperatur -= 5;
    }
    printf("\n Fertig!\n");
}

void printpreHeatMenu(){
    int sollTemp = 0;
    int startTemp = 25;
    printf("Vorheiz Menu\n");
    printf("==============\n\n");
    printf("Bitte gewuenschte Temperatur eingeben\n");
    scanf("%d",&sollTemp);
    printf("Aufheizen startet\n");
    while (startTemp <= sollTemp) {
        printf("\n Temperatur: %d", startTemp);
        startTemp += 1;
    }
    printf("\nVorheiz Temperatur ist erreicht von %d\n",startTemp-1);
}
void printPrintMenu(){

    printf("Druck Menu\n");
    printf("==============\n\n");
    printf("Ausgabe Auswahl File\n");
    files_suchen();
    printf("Gewueschtes File auswaehlen durch eingabe vom kompletten File Name\nNur .g und .gx Files sind erlaubt."
           " Dateiendung muss klein geschrieben sein\n");
    scanf("%s",fileName);
    endungpruefen(fileName);
    zeitTempLesen(firstLine);
    int sollTemp = temp;
    int startTemp = 25;
    int TimeDuration = zeit;
    while (startTemp <= sollTemp) {
        printf("\n Temperatur: %d\n", startTemp);
        startTemp += 5;
        }
    printf("Vorheiz Temperatur erreicht\n");
    while (TimeDuration >= 0) {
        printf("\n Time: %d", TimeDuration);
        printf("\n Spule dreht links");
        printf("\n Filament einziehen");
        printf("\n Druck im Gange");
        printf("\n Temperatur: %d\n", startTemp);
        TimeDuration -= 1000;
        }
    printf("%s",druck);
    printf("\n Druck ist beendet beendet in %d Sekunden, Gegenstand entfernen\n",zeit);
    TimeDuration = 0;
    sollTemp = 0;
}

void zeitTempLesen (char name[100]) // Auslesen der ersten Zeile Zeit und Temp plus umwandlung in int
{
    int i =0;
    int j = strlen(name);

    i = strcspn(name, semi);
    strncat(zeitTemp, name, i);
    i = i+1;
    strncat(tempTemp,strrev(name), j-i);
    strrev(tempTemp);
    temp = atoi(tempTemp);
    zeit = atoi(zeitTemp);
}
int endungpruefen(char name[30]) // Prüfen ob die Endung vom File .g oder .gx ist. Nur klein schreibung wird berücksichtigt
{
    if(strstr(name,".g")) {
        fileEinlesen(name);
        printf("File wurde eingelsen\n");
        return 0;
    }
    else if(strstr(name,".gx")) {
        fileEinlesen(name);
        printf("File wurde eingelsen\n");
        return 0;
    }
    else{
        printf("Falscher Dateityp ausgewaehlt bzw. Dateityp muss klein geschrieben werden.\n");
        return 1;
    }
}
void fileEinlesen(char name[30])  // Funktion zum Einlesen des Files. mit firstline (Zeit und Temperatur) Druck(der Rest)
{
    FILE *fp;
    char fileLocation[128] = "./FileStorage/";
    fp = fopen(strcat(fileLocation,name), "r");

    if(fp == NULL) {
        printf("Datei konnte nicht geoeffnet werden.\n Falsch geschrieben oder die Datei ist nicht im Ordner\n");
    }else {
        int buffersize = 255;
        char buffer[buffersize];
        int zeile = 1;
        while(fgets(buffer,buffersize,fp)){
            if(zeile == 1){
                strcat(firstLine,buffer);
            } else {
                strcat(druck,buffer);
            }
            zeile++;
        }



    }
}
int files_suchen() // Funktion zum suchen der vorhanden Files
{

    struct dirent *de;  // Pointer for directory entry

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir("./FileStorage");

    if (dr == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory");
        return 0;
    }

    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
    // for readdir()
    while ((de = readdir(dr)) != NULL) {
        printf("%s\n", de->d_name);
    }


    closedir(dr);

}