cmake_minimum_required(VERSION 3.20)
project(Projekt_C_Grundlagen C)

set(CMAKE_C_STANDARD 99)

add_executable(Projekt_C_Grundlagen main.c)
